#include <msp430.h> 

#define DELAY 50

char route[] = {'D', 'D', 'R', 'R', 'D', 'D', 'R', 'L', 'D', 'R', 'D', 'R', 'L', 'D', 'D', 'L', 'D', 'D', 'D', 'S'};

void vooruit(void)
{
    P1OUT &= ~(BIT3 | BIT4 | BIT5 | BIT7);
    P1OUT |= BIT4 | BIT7;
}

void achteruit(void)
{
    P1OUT &= ~(BIT3 | BIT4 | BIT5 | BIT7);
    P1OUT |= BIT5 | BIT3;
}

void keren(void)
{
    P1OUT &= ~(BIT3 | BIT4 | BIT5 | BIT7);
    P1OUT |= BIT5 | BIT7;
}

void rechts(void)
{
    P1OUT &= ~(BIT3 | BIT4 | BIT5 | BIT7);
    P1OUT |= BIT4;
}

void links(void)
{
    P1OUT &= ~(BIT3 | BIT4 | BIT5 | BIT7);
    P1OUT |= BIT7;
}

void stoppen(void)
{
    P1OUT &= ~(BIT3 | BIT4 | BIT5 | BIT7);
}

int Sensor_0(void)
{
    if((P2IN & BIT0) == 0)
    {
        return 0;
    }
    else if((P2IN & BIT0) != 0)
    {
        return 1;
    }
}

int Sensor_1(void)
{
    if((P2IN & BIT1) == 0)
    {
        return 0;
    }
    else if((P2IN & BIT1) != 0)
    {
        return 1;
    }
}

int Sensor_2(void)
{
    if((P2IN & BIT2) == 0)
    {
        return 0;
    }
    else if((P2IN & BIT2) != 0)
    {
        return 1;
    }
}

int Sensor_3(void)
{
    if((P2IN & BIT5) == 0)
    {
        return 0;
    }
    else if((P2IN & BIT5) != 0)
    {
        return 1;
    }
}
int Sensor_4(void)
{
    if((P2IN & BIT3) == 0)
    {
        return 0;
    }
    else if((P2IN & BIT3) != 0)
    {
        return 1;
    }
}

int Sensor_5(void)
{
    if((P2IN & BIT4) == 0)
    {
        return 0;
    }
    else if((P2IN & BIT4) != 0)
    {
        return 1;
    }
}

void blijfOpLijn(void)
{
    while((P1IN & BIT0) != 0)
    {
        keren();
        __delay_cycles(200);
    }

    while(Sensor_1() == 0 && Sensor_2() == 1 && Sensor_4() == 0)
    {
            vooruit();
            //__delay_cycles(DELAY);
    }
    while(Sensor_1() == 1 && Sensor_2() == 0 && Sensor_4() == 0)
    {
        rechts();
        //__delay_cycles(DELAY);
    }
    while(Sensor_1() == 0 && Sensor_2() == 0 && Sensor_4() == 0)
    {
        rechts();
        //__delay_cycles(DELAY);
    }
    while(Sensor_1() == 0 && Sensor_2() == 0 && Sensor_4() == 1)
    {
        links();
       // __delay_cycles(DELAY);
    }
    while(Sensor_1() == 0 && Sensor_2() == 0 && Sensor_4() == 0)
    {
        links();
        //__delay_cycles(DELAY);
    }
}

void rechtdoor(void)
{
    blijfOpLijn();
}
void linksaf(void)
{
    links();
    __delay_cycles(300);
    blijfOpLijn();
}
void rechtsaf(void)
{
    rechts();
    __delay_cycles(300);
    blijfOpLijn();
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    if (CALBC1_1MHZ==0xFF)  //
    {                   //
        while(1);       //  DCO instellen. Wordt gebruikt voor SMCLK
    }                   //
    BCSCTL1 = CALBC1_1MHZ;  //
    DCOCTL = CALDCO_1MHZ;   //

    TA0CTL = TASSEL_2 | ID_2 | MC_1; //taie mag niet aan staan als je outmod _7 gebruikt
    TA0CCR0 = 16;   // Frequency, moet laag blijven.
    TA0CCR1 = 13;    // Duty-cylce, mag niet hoger worden dan frequency
    TA0CCTL1 = OUTMOD_7;

    P1DIR |= (BIT2 | BIT6);      //
    P1SEL |= (BIT2 | BIT6);      //   instellen voor gebruik met timer
    P1SEL2 &= ~(BIT2 | BIT6);

    P1DIR |= (BIT3 | BIT4 | BIT5 | BIT7);
    P1DIR &= ~(BIT0 | BIT1);
    P1OUT &= ~(BIT3 | BIT4 | BIT5 | BIT7);
    P1REN |= BIT0 | BIT1;
    P1OUT &= ~(BIT0 | BIT1);

    P2DIR &= ~(BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5);
    P2REN |= (BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5);
    P2OUT |= (BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5);

    __enable_interrupt();

    while(1)
    {
        //Hoeken
        while(Sensor_0() == 1 && Sensor_1() == 1 && Sensor_2() == 1 && Sensor_3() == 0 && Sensor_4() == 0 && Sensor_5() == 0)
        {
           rechts();
           __delay_cycles(250);
        }
        while(Sensor_0() == 0 && Sensor_1() == 0 && Sensor_2() == 1 && Sensor_3() == 0 && Sensor_4() == 1 && Sensor_5() == 1)
        {
           links();
           __delay_cycles(250);
        }

        //Keuzes maken T-splitsing
        while(Sensor_0() == 1 && Sensor_1() == 1 && Sensor_2() == 1 && Sensor_3() == 0 && Sensor_4() == 1 && Sensor_5() == 1)
        {
            if(j==1)
                {
                    if(route[i] == 'D')
                    {
                        vooruit();
                        __delay_cycles(250);
                    }
                    if(route[i] == 'L')
                    {
                        links();
                        __delay_cycles(250);
                    }
                    if(route[i] == 'R')
                    {
                        rechts();
                        __delay_cycles(250);
                    }
                    if(route[i] == 'S')
                    {
                        stoppen();
                        while(1);
                    }
                    i++;
                    j=0;
                }
        }
        while(Sensor_0() == 0 && Sensor_1() == 0 && Sensor_2() == 1 && Sensor_3() == 1 && Sensor_4() == 1 && Sensor_5() == 1)
        {
            if(j==1)
                {
                    if(route[i] == 'D')
                    {
                        vooruit();
                        __delay_cycles(250);
                    }
                    if(route[i] == 'L')
                    {
                        links();
                        __delay_cycles(250);
                    }
                    if(route[i] == 'R')
                    {
                        rechts();
                        __delay_cycles(250);
                    }
                    if(route[i] == 'S')
                    {
                        stoppen();
                        while(1);
                    }
                    i++;
                    j=0;
                }
        }
        while(Sensor_0() == 0 && Sensor_1() == 0 && Sensor_2() == 1 && Sensor_3() == 1 && Sensor_4() == 1 && Sensor_5() == 0)
                {
                    if(j==1)
                        {
                            if(route[i] == 'D')
                            {
                                vooruit();
                                __delay_cycles(250);
                            }
                            if(route[i] == 'L')
                            {
                                links();
                                __delay_cycles(250);
                            }
                            if(route[i] == 'R')
                            {
                                rechts();
                                __delay_cycles(250);
                            }
                            if(route[i] == 'S')
                            {
                                stoppen();
                                while(1);
                            }
                            i++;
                            j=0;
                        }
                }
        while(Sensor_0() == 1 && Sensor_1() == 1 && Sensor_2() == 1 && Sensor_3() == 1 && Sensor_4() == 0 && Sensor_5() == 0)
        {
            if(j==1)
                {
                    if(route[i] == 'D')
                    {
                        vooruit();
                        __delay_cycles(250);
                    }
                    if(route[i] == 'L')
                    {
                        links();
                        __delay_cycles(250);
                    }
                    if(route[i] == 'R')
                    {
                        rechts();
                        __delay_cycles(250);
                    }
                    if(route[i] == 'S')
                    {
                        stoppen();
                        while(1);
                    }
                    i++;
                    j=0;
                }
        }
        while(Sensor_0() == 0 && Sensor_1() == 1 && Sensor_2() == 1 && Sensor_3() == 1 && Sensor_4() == 0 && Sensor_5() == 0)
                {
                    if(j==1)
                        {
                            if(route[i] == 'D')
                            {
                                vooruit();
                                __delay_cycles(250);
                            }
                            if(route[i] == 'L')
                            {
                                links();
                                __delay_cycles(250);
                            }
                            if(route[i] == 'R')
                            {
                                rechts();
                                __delay_cycles(250);
                            }
                            if(route[i] == 'S')
                            {
                                stoppen();
                                while(1);
                            }
                            i++;
                            j=0;
                        }
                }

        //Keuzes maken kruising
        while(Sensor_0() == 1 && Sensor_1() == 1 && Sensor_2() == 1 && Sensor_3() == 1 && Sensor_4() == 1 && Sensor_5() == 1)
        {
            if(j==1)
                {
                    if(route[i] == 'D')
                    {
                        vooruit();
                        __delay_cycles(250);
                    }
                    if(route[i] == 'L')
                    {
                        links();
                        __delay_cycles(250);
                    }
                    if(route[i] == 'R')
                    {
                        rechts();
                        __delay_cycles(250);
                    }
                    if(route[i] == 'S')
                    {
                        stoppen();
                        while(1);
                    }
                    i++;
                    j=0;
                }
        }
        j=1;
     }
}
